vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO wxWidgets/phoenix
    REF b567d1e6e3aff88f900d5ec4fb148531746b211d
    SHA512 c51cb778a4f8cc783350d1009ef5afbe2529d44120fd7e9b67ad08f4c56bc038413fa525e3a9467e9112407956582fc46f5d955b6bee974d674690d0f07e4cc4
    PATCHES
        0001-Patch-new-gtk-only-helper.patch
)

# We need a copy of the wxWidgets source code to build doxygen xml, the doxygen xml is used to generate the C++ code
# This should match the vcpkg port of wxWidgets
# THIS DOES NOT GET COMPILED
vcpkg_from_github(    
    OUT_SOURCE_PATH WX_SOURCE_PATH
    REPO wxWidgets/wxWidgets
    REF 97e99707c5d2271a70cb686720b48dbf34ced496 # v3.2.1
    SHA512 b47d3f4560f0ba24e95ce4fba0a807cc47807df57d54b70e22a34a5a15fc1e35ccedf1938203046c8950db9115ed09cb66fa1ca30b2e5f1b4c0d529a812497c4 
    HEAD_REF master
)

find_program(GIT NAMES git git.cmd)

file(COPY ${WX_SOURCE_PATH}/	DESTINATION ${SOURCE_PATH}/ext/wxWidgets/)
file(COPY ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt DESTINATION ${SOURCE_PATH})
file(COPY ${CMAKE_CURRENT_LIST_DIR}/cmake/ DESTINATION ${SOURCE_PATH}/cmake/)


# We need bash.exe for the wxPython preprocessor
if(CMAKE_HOST_WIN32)
    vcpkg_acquire_msys(MSYS_ROOT PACKAGES bash)
    vcpkg_add_to_path(PREPEND "${MSYS_ROOT}/usr/bin")

	set(Python3_FIND_REGISTRY "NEVER")
    set(PYTHON3_ROOT_DIR "${_VCPKG_INSTALLED_DIR}/${TARGET_TRIPLET}/tools/python3")
    vcpkg_add_to_path(PREPEND "${PYTHON3_ROOT_DIR}")
    vcpkg_add_to_path(PREPEND "${PYTHON3_ROOT_DIR}/scripts")

    set(ENV{PYTHONHOME} "${PYTHON3_ROOT_DIR}")
    set(ENV{PYTHONPATH} "${PYTHON3_ROOT_DIR}/DLLs;${PYTHON3_ROOT_DIR}/Lib")
endif()


find_package( Python3 COMPONENTS Interpreter REQUIRED )

vcpkg_execute_build_process(
    COMMAND ${Python3_EXECUTABLE} -m ensurepip
    WORKING_DIRECTORY "${SOURCE_PATH}"
    LOGNAME "prepare-ensurepip-${RELEASE_TRIPLET}"
)

vcpkg_execute_build_process(
    COMMAND ${Python3_EXECUTABLE} -m pip install -r ${SOURCE_PATH}/requirements.txt
    WORKING_DIRECTORY "${SOURCE_PATH}"
    LOGNAME "prepare-requirements-${RELEASE_TRIPLET}"
)

vcpkg_execute_build_process(
    COMMAND ${Python3_EXECUTABLE} ./build.py dox
    WORKING_DIRECTORY "${SOURCE_PATH}"
    LOGNAME "prepare-dox-${RELEASE_TRIPLET}"
)

vcpkg_execute_build_process(
    COMMAND ${Python3_EXECUTABLE} ./build.py etg --nodoc sip
    WORKING_DIRECTORY "${SOURCE_PATH}"
    LOGNAME "prepare-etg-${RELEASE_TRIPLET}"
)


vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
)

vcpkg_install_cmake()

file(INSTALL ${SOURCE_PATH}/wx/ DESTINATION ${CURRENT_PACKAGES_DIR}/tools/python3/Lib/site-packages/wx/)
file(INSTALL ${SOURCE_PATH}/wx/include/wxPython/ DESTINATION ${CURRENT_PACKAGES_DIR}/include/wxPython/)

# Per https://www.wxpython.org/pages/license/, using wxWindows Library License, no license in the source repo
file(INSTALL ${CMAKE_CURRENT_LIST_DIR}/copyright DESTINATION ${CURRENT_PACKAGES_DIR}/share/${PORT})